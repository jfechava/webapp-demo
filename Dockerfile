# Pull base image 
From tomcat:8-jre8 

LABEL maintainer=”jfechava@yahoo.com”

COPY webapp/target/webapp.war /usr/local/tomcat/webapps
